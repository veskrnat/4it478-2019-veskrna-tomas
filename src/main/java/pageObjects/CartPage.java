package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class CartPage {

    private WebDriver driver;

    @FindBy(id="hlb-view-cart-announce")
    private WebElement viewCartBtn;

    @FindBy(className="sc-product-title")
    private List<WebElement> productTitles;

    public CartPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void openCartContent() {
        viewCartBtn.click();
    }

    public String getNthProductTitle(int n) {
        return productTitles.get(n-1).getText();
    }
}
